package com.stolbunov;

public interface IBankFactory {
    IBank createRedBank();

    IBank createYellowBank();

    IBank createGreenBank();
}
