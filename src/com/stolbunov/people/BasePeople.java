package com.stolbunov.people;

import com.stolbunov.IPeople;
import com.stolbunov.banks_system.banks.IClient;

public abstract class BasePeople implements IClient, IPeople {
    private int passportData;
    private String name;

    protected BasePeople(String name) {
        this.name = name;
        passportData = (int) (Math.random() * 100000);
    }

    @Override
    public int getPassportData() {
        return passportData;
    }

    protected boolean dropTheCoin() {
        int randomNum = (int) (Math.random() * 100);
        return (randomNum % 2) == 0;
    }

    @Override
    public String getName() {
        return name;
    }
}
