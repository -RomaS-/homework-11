package com.stolbunov.people.simple_humas;

import com.stolbunov.IPeople;
import com.stolbunov.IPeopleFactory;

import java.util.ArrayList;
import java.util.List;

public class SimpleHumanFactory implements IPeopleFactory {
    private final int AMOUNT_NAME = 12;
    private int counterName = 0;
    private List<String> listNames;


    public SimpleHumanFactory() {
        listNames = createListNames();
    }

    @Override
    public IPeople create() {
        SimpleHuman simpleHuman = new SimpleHuman(listNames.get(counterName));
        nextName();
        return simpleHuman;
    }

    private List<String> createListNames() {
        List<String> names = new ArrayList<>(AMOUNT_NAME);
        names.add("Aurelia");
        names.add("Valeria");
        names.add("Darina");
        names.add("Jasmine");
        names.add("Elizabeth");
        names.add("Ksenia");
        names.add("Vladimir");
        names.add("Dmitriy");
        names.add("Maksim");
        names.add("Oleg");
        names.add("Ruslan");
        names.add("Fedor");
        return names;
    }

    private void nextName() {
        if (counterName < listNames.size() - 1) {
            counterName++;
        } else {
            counterName = 0;
        }
    }
}
