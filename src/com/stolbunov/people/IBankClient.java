package com.stolbunov.people;

import com.stolbunov.banks_system.banks.IClient;

public interface IBankClient {
    void giveCredit(IClient client);
}
