package com.stolbunov;

public interface IPeopleFactory {
    IPeople create();
}
