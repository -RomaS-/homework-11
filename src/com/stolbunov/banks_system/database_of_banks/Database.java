package com.stolbunov.banks_system.database_of_banks;

import com.stolbunov.banks_system.banks.IDatabase;
import com.stolbunov.banks_system.database_of_banks.exception.not_found.NotFoundException;

import java.util.HashMap;
import java.util.Map;

public class Database implements IDatabase {
    private static Database data;
    private Map<String, Map<Integer, Integer>> database;

    private Database() {
        database = new HashMap<>();
    }

    public static Database getInstance() {
        if (data == null) {
            data = new Database();
        }
        return data;
    }

    @Override
    public void addDatabase(String nameBank, int passportData, int sumCredit) {
        if (database.containsKey(nameBank)) {
            Map<Integer, Integer> bankDB = database.get(nameBank);
            bankDB.put(passportData, sumCredit);
        } else {
            Map<Integer, Integer> bankDB = new HashMap<>();
            bankDB.put(passportData, sumCredit);
            database.put(nameBank, bankDB);
        }
    }

    @Override
    public boolean isClientDB(String nameBank, int passportData) {
        Map<Integer, Integer> bankDB = database.get(nameBank);
        if (bankDB != null) {
            return bankDB.containsKey(passportData);
        } else {
            return false;
        }
    }

    @Override
    public Map<Integer, Integer> getAllClientWithDebts(String nameBank) {
        if (database.containsKey(nameBank)) {
            return database.get(nameBank);
        }
        throw new NotFoundException("The requested bank is not in the database. " +
                "Check the name of the bank or create a new entry in the database.");
    }
}
