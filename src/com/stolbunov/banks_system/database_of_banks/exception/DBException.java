package com.stolbunov.banks_system.database_of_banks.exception;

public class DBException extends RuntimeException {
    public DBException() {
        super();
    }

    public DBException(String message) {
        super(message);
    }
}
