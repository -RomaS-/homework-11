package com.stolbunov.banks_system.banks.green_bank;

import com.stolbunov.banks_system.banks.BaseBank;
import com.stolbunov.banks_system.banks.IClient;

import java.util.Map;

public class GreenBank extends BaseBank {
    private final String NAME_BANK = "Green Bank";
    private int STANDARD_SUM_CREDIT = 156;
    private final float INTEREST_RATE = 0.2F;

    @Override
    public void giveCredit(IClient client) {
        super.giveCredit(client);
        if (computer.processAddClient(this, STANDARD_SUM_CREDIT, client)) {
            client.takeCredit(NAME_BANK, STANDARD_SUM_CREDIT);
        }
    }

    @Override
    public Map<Integer, Integer> getMapDebtors() {
        return computer.getAllDebtors(this, INTEREST_RATE);
    }

    @Override
    public String getNameBank() {
        return NAME_BANK;
    }

    @Override
    public void printMapDebtors() {
        printMap(this);
    }
}
