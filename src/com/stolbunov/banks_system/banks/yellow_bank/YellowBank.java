package com.stolbunov.banks_system.banks.yellow_bank;

import com.stolbunov.banks_system.banks.BaseBank;
import com.stolbunov.banks_system.banks.IClient;
import com.stolbunov.banks_system.computer_banks.IBankComputer;

import java.util.Map;
import java.util.function.BiConsumer;

public class YellowBank extends BaseBank {
    private final String NAME_BANK = "Yellow Bank";
    private final int STANDARD_SUM_CREDIT = 354;
    private final float INTEREST_RATE = 0.4F;

    @Override
    public void giveCredit(IClient client) {
        super.giveCredit(client);
        if (computer.processAddClient(this, STANDARD_SUM_CREDIT, client)) {
            client.takeCredit(NAME_BANK, STANDARD_SUM_CREDIT);
        }
    }


    @Override
    public Map<Integer, Integer> getMapDebtors() {
        return computer.getAllDebtors(this, INTEREST_RATE);
    }

    @Override
    public String getNameBank() {
        return NAME_BANK;
    }

    @Override
    public void printMapDebtors() {
        printMap(this);
    }


}
