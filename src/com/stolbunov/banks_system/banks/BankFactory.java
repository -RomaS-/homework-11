package com.stolbunov.banks_system.banks;

import com.stolbunov.IBank;
import com.stolbunov.IBankFactory;
import com.stolbunov.banks_system.banks.green_bank.GreenBank;
import com.stolbunov.banks_system.banks.red_bank.RedBank;
import com.stolbunov.banks_system.banks.yellow_bank.YellowBank;

public class BankFactory implements IBankFactory {

    @Override
    public IBank createRedBank() {
        return new RedBank();
    }

    @Override
    public IBank createYellowBank() {
        return new YellowBank();
    }

    @Override
    public IBank createGreenBank() {
        return new GreenBank();
    }
}
