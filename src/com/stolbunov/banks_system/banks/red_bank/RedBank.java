package com.stolbunov.banks_system.banks.red_bank;

import com.stolbunov.banks_system.banks.BaseBank;
import com.stolbunov.banks_system.banks.IClient;

import java.util.Map;

public class RedBank extends BaseBank {
    private final String NAME_BANK = "Red Bank";
    private int STANDARD_SUM_CREDIT = 1565;
    private final float INTEREST_RATE = 0.6F;

    @Override
    public void giveCredit(IClient client) {
        super.giveCredit(client);
        if (computer.processAddClient(this, STANDARD_SUM_CREDIT, client)) {
            client.takeCredit(NAME_BANK, STANDARD_SUM_CREDIT);
        }
    }

    @Override
    public Map<Integer, Integer> getMapDebtors() {
        return computer.getAllDebtors(this, INTEREST_RATE);
    }

    @Override
    public String getNameBank() {
        return NAME_BANK;
    }

    @Override
    public void printMapDebtors() {
        printMap(this);
    }
}
