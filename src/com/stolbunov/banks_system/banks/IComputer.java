package com.stolbunov.banks_system.banks;

import com.stolbunov.banks_system.computer_banks.IBankComputer;

import java.util.Map;

public interface IComputer {
    boolean processAddClient(IBankComputer bank, int sumCredit, IClient client);

    Map<Integer, Integer> getAllDebtors(IBankComputer bank, float interestRate);

    Map<Integer, Integer> getAllDebtors(IBankComputer bank);
}
