package com.stolbunov.banks_system.computer_banks;

import com.stolbunov.IComputerTime;
import com.stolbunov.banks_system.banks.IClient;
import com.stolbunov.banks_system.banks.IComputer;
import com.stolbunov.banks_system.banks.IDatabase;
import com.stolbunov.banks_system.database_of_banks.Database;

import java.util.Map;
import java.util.Set;

public class Computer implements IComputer, IComputerTime {
    private static int goneTime;
    private IDatabase database;
    private final String TEXT_TIME = "%n<<Computer>> After %d years";
    private final String SUCCESSFUL_OPERATION = "<<%1$s>> %nThe operation of issuing the loan was completed successfully!" +
            "%nClient <<%2$s>> got credit for the amount of <<%3$d>>%n";
    private final String DENIAL_CREDIT = "<<%1$s>> %nSorry but you are denied credit! " +
            "%nYou already took out a loan from us: %nClient <<%2$s>> amount of credit <<%3$d>>%n";

    public Computer() {
        database = Database.getInstance();
    }

    @Override
    public boolean processAddClient(IBankComputer bank, int sumCredit, IClient client) {
        int passportData = client.getPassportData();
        String nameBank = bank.getNameBank();
        if (!database.isClientDB(nameBank, passportData)) {
            messageStateOperation(SUCCESSFUL_OPERATION, nameBank, client, sumCredit);
            database.addDatabase(nameBank, passportData, sumCredit);
            return true;
        } else {
            messageStateOperation(DENIAL_CREDIT, nameBank, client, sumCredit);
            return false;
        }
    }

    @Override
    public Map<Integer, Integer> getAllDebtors(IBankComputer bank) {
        return getAllDebtors(bank, 0);
    }

    @Override
    public Map<Integer, Integer> getAllDebtors(IBankComputer bank, float interestRate) {
        Map<Integer, Integer> map = database.getAllClientWithDebts(bank.getNameBank());
        if (goneTime != 0) {
            return considerDebts(map, interestRate);
        }
        return map;
    }

    private Map<Integer, Integer> considerDebts(Map<Integer, Integer> map, float interestRate) {
        Set<Integer> clients = map.keySet();
        for (Integer client : clients) {
            int newDebt = map.get(client);
            int percentGoneTime = (int) (newDebt * interestRate) * goneTime;
            newDebt += percentGoneTime;
            map.put(client, newDebt);
        }
        return map;
    }

    @Override
    public void setGoneTime(int goneTime) {
        Computer.goneTime = goneTime;
        System.out.println(String.format(TEXT_TIME, goneTime));
    }

    private void messageStateOperation(String text, String nameBank, IClient client, int sumCredit) {
        System.out.println(String.format(text, nameBank, client.getName(), sumCredit));
    }
}
