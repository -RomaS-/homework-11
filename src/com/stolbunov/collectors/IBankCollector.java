package com.stolbunov.collectors;

import java.util.Map;

public interface IBankCollector {
    String NAME_ORGANIZATION = "%n%20s";
    String BLANK = "%-12s%-15s%-5s";
    String TITLE = String.format(BLANK, "Name", "Passport Data", "Sum Credit");
    String LINE = "---------------------------------------";

    Map<Integer, Integer> getMapDebtors();


    static void printMessage(String name, int passportData, int sumCredit) {
        System.out.println(String.format(BLANK, name, passportData, sumCredit));
    }
}
