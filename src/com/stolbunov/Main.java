package com.stolbunov;

import com.stolbunov.banks_system.banks.BankFactory;
import com.stolbunov.banks_system.computer_banks.Computer;
import com.stolbunov.collectors.Collector;
import com.stolbunov.people.IBankClient;
import com.stolbunov.people.simple_humas.SimpleHumanFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.stolbunov.collectors.IBankCollector.LINE;
import static com.stolbunov.collectors.IBankCollector.NAME_ORGANIZATION;
import static com.stolbunov.collectors.IBankCollector.TITLE;

public class Main {
    private static final int AMOUNT_PEOPLE = 10;
    private static final int AMOUNT_BANK = 3;
    private static final int GONE_TIME = 4;
    private static final String LIST_DEBTORS = "%nList of debtors %s : ";
    private static final String TEXT_BEFORE_SOME_TIME = " in the beginning";
    private static final String TEXT_AFTER_SOME_TIME = " redeemed by Collector after %s years";
    private static List<IPeople> people;
    private static List<IBank> banks;
    private static ICollector collector;
    private static IComputerTime time;

    public static void main(String[] args) {
        collector = new Collector();
        time = new Computer();

        people = createClients();
        banks = createBank();
        clientsGoToBanks();
        printMapDebtors(TEXT_BEFORE_SOME_TIME);
        counterYears();
        purchaseMapDebtors();
        printMapDebtors(String.format(TEXT_AFTER_SOME_TIME, GONE_TIME));
        printTotalDebtClient();
    }

    private static void purchaseMapDebtors() {
        for (IBank bank : banks) {
            Map<Integer, Integer> mapDebtors = bank.getMapDebtors();
            collector.takeMapDebtors(mapDebtors);
        }
    }

    private static void counterYears() {
        time.setGoneTime(GONE_TIME);
    }

    private static void clientsGoToBanks() {
        for (IPeople people : people) {
            for (IBankClient bank : banks) {
                people.goToBank(bank);
            }

            for (IBankClient bank : banks) {
                people.goToBank(bank);
            }
        }
    }

    private static List<IPeople> createClients() {
        List<IPeople> clients = new ArrayList<>();
        IPeopleFactory peopleFactory = new SimpleHumanFactory();
        for (int i = 0; i < AMOUNT_PEOPLE; i++) {
            clients.add(peopleFactory.create());
        }
        return clients;
    }

    private static List<IBank> createBank() {
        List<IBank> banks = new ArrayList<>(AMOUNT_BANK);
        IBankFactory factory = new BankFactory();
        banks.add(factory.createRedBank());
        banks.add(factory.createYellowBank());
        banks.add(factory.createGreenBank());
        return banks;
    }

    private static void printMapDebtors(String text) {
        System.out.println(String.format(LIST_DEBTORS, text));
        for (IBank bank : banks) {
            bank.printMapDebtors();
        }
    }

    private static void printTotalDebtClient() {
        System.out.println(String.format(NAME_ORGANIZATION, collector.getNAME()));
        System.out.println(TITLE);
        System.out.println(LINE);
        for (IPeople client : people) {
            collector.printTotalDebtClient(client);
        }
    }
}
