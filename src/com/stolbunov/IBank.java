package com.stolbunov;

import com.stolbunov.collectors.IBankCollector;
import com.stolbunov.people.IBankClient;

public interface IBank extends IBankClient, IBankCollector {
    void printMapDebtors();
}
